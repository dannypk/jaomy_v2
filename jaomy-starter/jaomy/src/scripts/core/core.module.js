/**
 * Core Module
 *
 * This is the shared core module. Include all angular and global shared modules
 * into this core module.
 *
 * @namespace Core
 */
angular
  .module('ionic-starter.core', [
    /* Angular modules & Ionic*/
    'ionic',
    'ngCordova',
    'pascalprecht.translate'
  ]);
