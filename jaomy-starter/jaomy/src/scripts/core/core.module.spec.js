/**
 * Core Module example test.
 *
 * @namespace Tests
 * @memberOf Core
 */
describe('Core Module', function() {
  beforeEach(function() {
    module('ionic-starter.core');
  });

  // Should return true.
  it('true should be true', function () {
    expect(true).toBe(true);
  });
});
