/**
* Pocket Module
*
* Inside this module you can play with pockets - add, edit, remove.
*
* @namespace Pocket
*/
angular
  .module('ionic-starter.pocket', [
    'ionic-starter.core'
  ]);
