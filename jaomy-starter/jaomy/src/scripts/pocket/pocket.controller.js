/**
* Pocket Module
*
* Inside this module you can play with pockets - add, edit, remove.
*
* @namespace Pocket
*/
angular
  .module('ionic-starter.home')
  .controller('PocketController', PocketController);

/**
 * @name PocketController
 * @desc Controller for the pocket view.
 * @namespace PocketController
 * @memberOf Pocket
 */
function PocketController($scope) {
  var vm = $scope;
  this.pocket = {};
  this.havePockets = false;

  this.addPocket = function(item)
  {
    console.log(item);
    this.havePockets = true;
    //add pocket here.
  };
  vm.name = 'Pocket';

}
