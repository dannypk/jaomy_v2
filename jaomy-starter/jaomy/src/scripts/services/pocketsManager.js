angular.module('ionic-starter.services', [])

.service('pocketsManager',function(){
            /*
            var schema = new Schema({
                name:       String,
                date:       Date,
                amount:     Number,
                note:       String,
                people:     []       
            });
            */
            return{
                    pocketsAll:[],
                    

                    init: function(successCallback , errorCallback){
                        this.pocketsAll=[];
                        /*dummy data should be replaced later by restApi responce - ask igor */
                        var qtyOfPockets=8;
                        for(var i=0;i<qtyOfPockets;i++){
                            var pocketItem = 
                            {  
                                name: 'Pocket '+i,
                                date: Date.now(),
                                amount: ((100+1.5)*i/2.345).toFixed(2),
                                note: 'Some user description of this pocket',
                                people: ['Mike','Daniel','Igor']                 
                            };
                            
                            this.pocketsAll.push(pocketItem);
                        }

                        successCallback();

                    }
            };
    });
    //TEST