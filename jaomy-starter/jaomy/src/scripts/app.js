/**
 * App Entry Point
 *
 * @namespace App
 */
angular
    .module('ionic-starter', [

    /* Shared modules */
    'ionic-starter.core',
    'ionic-starter.templates',

    /* Application modules */
    'ionic-starter.home',
    'ionic-starter.settings',
    'ionic-starter.pocket',

    /* Services */
    'ionic-starter.services'
  ])
  .config(configure)
  .run(runBlock);

/**
 * @name configure
 * @namespace Configuration
 * @desc Application wide configuration
 * @memberOf App
 */
function configure($stateProvider, $urlRouterProvider, $translateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      views: {
        home: {
          templateUrl: 'home/home.html'
        }
      },
      controller: 'HomeController',
      controllerAs: 'vm'
    })
    .state('settings', {
      url: '/settings',
      views: {
        settings: {
          templateUrl: 'settings/settings.html'
        }
      },
      controller: 'SettingsController',
      controllerAs: 'vm'
    })
    .state('pocket', {
      url: '/addPocket',
      views: {
        settings: {
          templateUrl: 'pocket/addPocket.html'
        }
      },
      controller: 'PocketController',
      controllerAs: 'vm'
    });

  // Route fallback
  $urlRouterProvider.otherwise('/');

  // Configure language.
  $translateProvider.determinePreferredLanguage();
  $translateProvider.fallbackLanguage('en_US');
}

/**
 * @name runBlock
 * @namespace Configuration
 * @desc Called the first time when the app starts. Initialize app wide config here.
 * @param {Ionic} $ionicPlatform Ionic platform
 * @memberOf App
 */
function runBlock($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar
    // above the keyboard for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}
