/**
* Home Module
*
* This module is the first screen you see, when you start the app.
*
* @namespace Home
*/
angular
  .module('ionic-starter.settings', [
    'ionic-starter.core'
  ]);
