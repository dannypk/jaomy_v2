/**
* Home Module
*
* This module is the first screen you see, when you start the app.
*
* @namespace Home
*/
angular
  .module('ionic-starter.home')
  .controller('SettingsController', SettingsController);

/**
 * @name HomeController
 * @desc Controller for the home view.
 * @namespace HomeController
 * @memberOf Home
 */
function SettingsController($scope) {
  var vm = $scope;

  vm.name = 'Settings';
}
