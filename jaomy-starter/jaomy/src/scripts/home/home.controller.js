/**
* Home Module
*
* This module is the first screen you see, when you start the app.
*
* @namespace Home
*/
angular
  .module('ionic-starter.home')
  .controller('HomeController', HomeController);

/**
 * @name HomeController
 * @desc Controller for the home view.
 * @namespace HomeController
 * @memberOf Home
 */
function HomeController($scope,pocketsManager) {
  var vm = $scope;
  vm.pockets=[];
  pocketsManager.init(
          function(){
              vm.pockets=pocketsManager.pocketsAll;
          },
          function(){ 
              console.log('Init of pocketsManager was interupted.');
          }
  );
  vm.name = 'Jaomy';
}
