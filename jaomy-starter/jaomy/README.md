# Ionic Starter

## Important Notes

Never commit into this project unless it's to improve the boilerplate.
Just clone this repo to use it as a starting point and push your project into a
different repo.

## Setup

### Step 1

Clone this project into your new app folder.

```
git clone https://gitlab.bliphead.com:80/bliphead/ionic-starter.git
```

Run
```
npm install && bower install
```

### Step 2

Change APP_NAME to your application name in:

* package.json
* www/index.html
* scripts/app.js
* scripts/core/core.js
* ionic.project

### Setp 3

Run ionic serve to check if everthing works properly.

## Gulp

The following gulp commands are available:

Use this command to check if your code has a high quality standard.
Never deploy an app if this command still produces errors.
```
gulp lint
```

Compile your javascript files manually.
```
gulp scripts
```

To run tests
```
gulp test
```

Use this command during development.
```
ionic serve
```

## Ionic

Splashscreens:
http://ionicframework.com/blog/automating-icons-and-splash-screens/

## Notes

We use the following coding guidelines for this angular project:
https://github.com/johnpapa/angularjs-styleguide

Testing guide:
http://www.smashingmagazine.com/2014/10/07/introduction-to-unit-testing-in-angularjs/

Work in the scripts folder and not in the www/js folder. Into this folder
the scripts folder will be compiled into.

Try to use bower to install thiry-party dependencies.

Naming convention:
```
/**
* recommended
*/

// controllers
avengers.controller.js
avengers.controller.spec.js

// services/factories
logger.service.js
logger.service.spec.js

// constants
constants.js

// module definition
avengers.module.js

// routes
avengers.routes.js
avengers.routes.spec.js

// configuration
avengers.config.js

// directives
avenger-profile.directive.js
avenger-profile.directive.spec.js
```
