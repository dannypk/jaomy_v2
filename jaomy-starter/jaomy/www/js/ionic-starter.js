(function(){
/**
 * App Entry Point
 *
 * @namespace App
 */
angular
    .module('ionic-starter', [

    /* Shared modules */
    'ionic-starter.core',
    'ionic-starter.templates',

    /* Application modules */
    'ionic-starter.home',
    'ionic-starter.settings',
    
    /* Services */
    'ionic-starter.services'
  ])
  .config(configure)
  .run(runBlock);

/**
 * @name configure
 * @namespace Configuration
 * @desc Application wide configuration
 * @memberOf App
 */
function configure($stateProvider, $urlRouterProvider, $translateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      views: {
        home: {
          templateUrl: 'home/home.html'
        }
      },
      controller: 'HomeController',
      controllerAs: 'vm'
    })
    .state('settings', {
      url: '/settings',
      views: {
        settings: {
          templateUrl: 'settings/settings.html'
        }
      },
      controller: 'SettingsController',
      controllerAs: 'vm'
    });    

  // Route fallback
  $urlRouterProvider.otherwise('/');

  // Configure language.
  $translateProvider.determinePreferredLanguage();
  $translateProvider.fallbackLanguage('en_US');
}
configure.$inject = ["$stateProvider", "$urlRouterProvider", "$translateProvider"];

/**
 * @name runBlock
 * @namespace Configuration
 * @desc Called the first time when the app starts. Initialize app wide config here.
 * @param {Ionic} $ionicPlatform Ionic platform
 * @memberOf App
 */
function runBlock($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar
    // above the keyboard for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}
runBlock.$inject = ["$ionicPlatform"];

})();
(function(){
/**
 * Core Module
 *
 * This is the shared core module. Include all angular and global shared modules
 * into this core module.
 *
 * @namespace Core
 */
angular
  .module('ionic-starter.core', [
    /* Angular modules & Ionic*/
    'ionic',
    'ngCordova',
    'pascalprecht.translate'
  ]);

})();
(function(){
/**
* Home Module
*
* This module is the first screen you see, when you start the app.
*
* @namespace Home
*/
angular
  .module('ionic-starter.home', [
    'ionic-starter.core'
  ]);

})();
(function(){
/**
* Home Module
*
* This module is the first screen you see, when you start the app.
*
* @namespace Home
*/
angular
  .module('ionic-starter.settings', [
    'ionic-starter.core'
  ]);

})();
(function(){
/**
* Home Module
*
* This module is the first screen you see, when you start the app.
*
* @namespace Home
*/
angular
  .module('ionic-starter.home')
  .controller('HomeController', HomeController);

/**
 * @name HomeController
 * @desc Controller for the home view.
 * @namespace HomeController
 * @memberOf Home
 */
function HomeController($scope,pocketsManager) {
  var vm = $scope;
  vm.pockets=[];
  pocketsManager.init(
          function(){
              vm.pockets=pocketsManager.pocketsAll;
          },
          function(){ 
              console.log('Init of pocketsManager was interupted.');
          }
  );
  vm.name = 'Jaomy';
}
HomeController.$inject = ["$scope", "pocketsManager"];

})();
(function(){
angular.module('ionic-starter.services', [])

.service('pocketsManager',function(){
            /*
            var schema = new Schema({
                name:       String,
                date:       Date,
                amount:     Number,
                note:       String,
                people:     []       
            });
            */
            return{
                    pocketsAll:[],
                    

                    init: function(successCallback , errorCallback){
                        this.pocketsAll=[];
                        /*dummy data should be replaced later by restApi responce - ask igor */
                        var qtyOfPockets=8;
                        for(var i=0;i<qtyOfPockets;i++){
                            var pocketItem = 
                            {  
                                name: 'Pocket '+i,
                                date: Date.now(),
                                amount: ((100+1.5)*i/2.345).toFixed(2),
                                note: 'Some user description of this pocket',
                                people: ['Mike','Daniel','Igor']                 
                            };
                            
                            this.pocketsAll.push(pocketItem);
                        }

                        successCallback();

                    }
            };
    });
    //TEST
})();
(function(){
/**
* Home Module
*
* This module is the first screen you see, when you start the app.
*
* @namespace Home
*/
angular
  .module('ionic-starter.home')
  .controller('SettingsController', SettingsController);

/**
 * @name HomeController
 * @desc Controller for the home view.
 * @namespace HomeController
 * @memberOf Home
 */
function SettingsController($scope) {
  var vm = $scope;

  vm.name = 'Settings';
}
SettingsController.$inject = ["$scope"];

})();