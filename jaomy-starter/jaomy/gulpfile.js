var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var ngAnnotate = require('gulp-ng-annotate');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var pkg = require('./package.json');
var karma = require('gulp-karma');
var header = require('gulp-header');
var footer = require('gulp-footer');
var uglify = require('gulp-uglify');
var htmlreplace = require('gulp-html-replace');
var del = require('del');
var ngTemplates = require('gulp-ng-templates');
var htmlmin = require('gulp-htmlmin');

var paths = {
  sass: ['./src/scss/**/*.scss'],
  scripts: ['./src/scripts/*.js', './src/scripts/**/*.module.js', './src/scripts/**/*.js','!./src/scripts/**/*.spec.js'],
  tests: ['./src/scripts/**/*.spec.js']
};

gulp.task('default', ['sass', 'lint', 'scripts', 'templates','html']);

gulp.task('templates', function(done) {
  gulp.src('./src/scripts/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(ngTemplates(pkg.name + '.templates', 'templates.js'))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./www/js/'))
    .on('end', done);
});

gulp.task('lint', function(done) {
  gulp.src(paths.scripts)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .on('end', done);
});

gulp.task('scripts', function(done) {
  gulp.src(paths.scripts)
    .pipe(ngAnnotate())
    .pipe(header('(function(){\n'))
    .pipe(footer('\n})();'))
    .pipe(concat(pkg.name + '.js'))
    .pipe(gulp.dest('./www/js/'))
    .on('end', done);
});

gulp.task('clean', function(done) {
  del(['./www/js', './www/css'], done);
});

gulp.task('build', ['clean', 'scripts', 'sass', 'templates'], function() {
  // Compile scripts
  gulp.src('./www/js/' + pkg.name + '.js')
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest('./www/js/'));

  // Compile html
  gulp.src('./src/index.html')
    .pipe(htmlreplace({
      'js': 'js/' + pkg.name + '.min.js'
    }))
    .pipe(gulp.dest('./www/'));
});

gulp.task('sass', function(done) {
  gulp.src('./src/scss/ionic.app.scss')
    .pipe(sass())
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('html', function(done) {
  gulp.src('./src/*.html')
    .pipe(gulp.dest('./www/'))
    .on('end', done);
});

gulp.task('test', function(done) {
  gulp.src('./dummy')
    .pipe(karma({
      configFile: 'karma.conf.js',
      action: 'run'
    }))
    .on('error', function(err) {
      // Make sure failed tests cause gulp to exit non-zero
      console.log(err);
      this.emit('end'); //instead of erroring the stream, end it
    })
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.scripts, ['lint']);
  gulp.watch(paths.tests, ['test']);
  gulp.watch('./src/scripts/**/*.html', ['templates']);
  gulp.watch('./src/*.html', ['html']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
